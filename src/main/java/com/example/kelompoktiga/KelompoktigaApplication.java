package com.example.kelompoktiga;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KelompoktigaApplication {

	public static void main(String[] args) {
		SpringApplication.run(KelompoktigaApplication.class, args);
	}

}
